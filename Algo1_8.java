package com.pattraporn.algo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Pattrapon N
 */
public class Algo1_8 {
    public static int [] reverse(int[] arr){
        int arrCount = arr.length;
        for(int i = 0 ; i < arrCount / 2 ; i++){
            int a = arr[arrCount -1 -i];
            arr[arrCount - 1 - i ] = arr[i];
            arr[i]=a;
           
            
        }
        
            System.out.print(Arrays.toString(arr));
        
         return arr ;
    }
   
    public static void main(String[] args) {
         Scanner sc = new Scanner(System.in);
         int size = sc.nextInt();
         int[] array = new int[size];

        for(int i =0; i<size; i++)
        {
            array[i] = sc.nextInt();
        }                       
        reverse(array);
    }
}
